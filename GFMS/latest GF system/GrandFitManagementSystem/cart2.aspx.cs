﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace GrandFitManagementSystem
{
    public partial class cart2 : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection("server=WS141\\MSSQLSERVER2012;database=hrmdb;user id=hrmdbadmin;password=sqlserver");
        protected void Page_Load(object sender, EventArgs e)
        {
            date.Text = DateTime.Now.ToString("dd MMM yyyy");

            if(!IsPostBack)
            {
                if(Request.QueryString["ID"] != null)
                {
                    Add(Convert.ToInt32(Request.QueryString["quant"]));
                }
                GridView1.DataSource = GetDataSetCart();
                GridView1.DataBind();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("readymadeCheckout.aspx");
        }

        public DataSet GetDataSetCart()
        {
            if (Session["cart2"] == null)
            {
                DataSet ds = new DataSet();
                DataColumn colID = new DataColumn("ID_PRODUCT", System.Type.GetType("System.Int32"), "");
                DataTable dt = new DataTable("CART");
                dt.Columns.Add(colID);
                dt.Columns.Add("NAME_PRODUCT", System.Type.GetType("System.String"), "");
                dt.Columns.Add("QUANTITY", System.Type.GetType("System.Int32"), "");
                dt.Columns.Add("VALUE", System.Type.GetType("System.Double"), "");
                dt.Columns.Add("SUBTOTAL", System.Type.GetType("System.Double"), "QUANTITY*VALUE");
                dt.Columns.Add("TOTAL", System.Type.GetType("System.Double"), "SUM(SUBTOTAL)");
                // key field

                DataColumn[] keys = new DataColumn[1];
                keys[0] = colID;
                dt.PrimaryKey = keys;
                ds.Tables.Add(dt);
                Session["cart2"] = ds;
                return ds;
            }
            else
            {
                return (Session["cart2"] as DataSet);
            }

        }

        private void Add(int quantity)
        {
            
            string aSQL = "SELECT NAME_PRODUCT, VALUE_PRODUCT FROM PRODUCT WHERE ID_PRODUCT=" + Request.QueryString["ID"].ToString();
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(aSQL, con);
                SqlDataReader reader = cmd.ExecuteReader();
                reader.Read();

                DataTable dt = GetDataSetCart().Tables[0];

                DataRow row = dt.Rows.Find(Request.QueryString["ID"]);

                if(row == null)
                {
                    row = dt.NewRow();
                    row["ID_PRODUCT"] = Request.QueryString["ID"];
                    row["NAME_PRODUCT"] = reader["NAME_PRODUCT"];
                    row["QUANTITY"] = quantity;
                    row["VALUE"] = reader["VALUE_PRODUCT"];
                    dt.Rows.Add(row);

                }
                else
                {
                    int qtd = Convert.ToInt32(row["QUANTITY"]);
                    qtd = qtd + quantity;
                    row["QUANTITY"] = qtd;

                }
            }
                finally 
            {
                con.Close();
            }
            
        }

        private void Remove (int aId)
        {
            DataSet ds = GetDataSetCart();
            DataRow row = ds.Tables[0].Rows.Find(aId);
            if(row != null)
            {
                ds.Tables[0].Rows.Remove(row);
                ds.AcceptChanges();
                GridView1.DataSource = GetDataSetCart();
                GridView1.DataBind();
            }
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Remove(Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value));
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if(e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[3].Text = "Total";
                e.Row.Cells[3].Font.Bold = true;
                e.Row.Cells[4].Text = String.Format("{0:c}",GetDataSetCart().Tables[0].Rows[0]["TOTAL"]);
            }
        }

        private void Finish()
        {
            SqlConnection con = new SqlConnection("server=WS141\\MSSQLSERVER2012;database=hrmdb;user id=hrmdbadmin;password=sqlserver");
            try
            {
                SqlCommand cmd = new SqlCommand("sp_InsertSales", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Date", SqlDbType.DateTime, 0, "Date_Sales").Value = DateTime.Now;
                cmd.Parameters.Add("@Value", SqlDbType.Decimal, 18, "Value_Sales").Value = GetDataSetCart().Tables[0].Rows[0]["TOTAL"];
                cmd.Parameters.Add("@UserID", SqlDbType.Int, 0, "ID_User").Value = Convert.ToInt32(1);
                con.Open();

                int aKey = Convert.ToInt32(cmd.ExecuteScalar());
                foreach(DataRow row in GetDataSetCart().Tables[0].Rows)
                {

                    SqlCommand cmdItem = new SqlCommand("sp_InsertItemsSales", con);
                    cmdItem.CommandType = CommandType.StoredProcedure;
                    cmdItem.Parameters.Add("@IDSales", SqlDbType.Int, 0, "Date_Sales").Value = aKey;
                    cmdItem.Parameters.Add("@IDProduct", SqlDbType.Int, 0, "ID_Product").Value = row["ID_PRODUCT"];
                    cmdItem.Parameters.Add("@ValueProduct", SqlDbType.Decimal, 18, "Value_Product").Value = row["VALUE"];
                    cmdItem.Parameters.Add("@Quantity", SqlDbType.Int, 0, "Quantity").Value = row["QUANTITY"];
                    cmdItem.ExecuteNonQuery();
                }
                
            }
            finally
            {
                con.Close();
            }
        }

        protected void Button2_Click1(object sender, EventArgs e)
        {
            Finish();

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            
        }
        
       
    }
}