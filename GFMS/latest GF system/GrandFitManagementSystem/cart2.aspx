﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="cart2.aspx.cs" Inherits="GrandFitManagementSystem.cart2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					GrandFit Invoice <small>order details</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="readymadeCheckout.aspx">
								Search Product
							</a>
                            <i class="fa fa-angle-right"></i>
						</li>
                        <li>
							<a href="rmSeachResult.aspx">
								Choose Product
							</a>
                            <i class="fa fa-angle-right"></i>
						</li>
                        <li>
							<a href="cart1.aspx">
								Set Details
							</a>
                            <i class="fa fa-angle-right"></i>
						</li>
                        <li>
							<a href="#">
								Readymade Invoice
							</a>
						</li>

						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->

            <!-- BEGIN PAGE CONTENT-->
    <form runat="server">
        				<div class="row invoice-logo">
					<div class="col-xs-6 invoice-logo-space">
						<img src="assets/img/invoice/walmart.png" alt=""/>
					</div>
					<div class="col-xs-6">
						<h3>
                            Shop 301, 7 Bunglows, Versova, 
							<span class="muted">
                                Andheri West, Mumbai - 400061
							</span>
						</h3>
                        <h4>
                            Tel: 9919786070 / 528491 
                        </h4>
                        <h4>
                            mehraaz@grandfit.com
                        </h4>
                        <h4><asp:Label ID="date" runat="server" Text="Label"></asp:Label></h4>
					</div>
				</div>
				<hr/>
				<div class="row">
					<div class="col-xs-4">
						<h3>Client:</h3>
                        
                        </div>
                <div class="col-xs-4">
						<h3>About Us:</h3>
                    </div>
                <div class="col-xs-4" >
						<h3>Payment Details:</h3>
                    </div>
                    </div>
        <hr />

            <div class="portlet-body">
        <asp:GridView ID="GridView1" runat="server" CssClass="table-responsive table table-striped table-bordered table-hover" AutoGenerateColumns="False" DataKeyNames="ID_PRODUCT" OnRowDataBound="GridView1_RowDataBound" OnRowDeleting="GridView1_RowDeleting" ShowFooter="True">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" />
                <asp:TemplateField HeaderText="Name Product">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBoxName" runat="server" Text='<%# Bind("NAME_PRODUCT") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("NAME_PRODUCT") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Quantity">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBoxQuantity" runat="server" Text='<%# Bind("QUANTITY") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("QUANTITY") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Value">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBoxValue" runat="server" Text='<%# Bind("VALUE") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("VALUE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SubTotal">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBoxSubTotal" runat="server" Text='<%# Bind("SUBTOTAL") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("SUBTOTAL") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

        <br />

        <asp:Button ID="Button1" runat="server" Text="Add Product" CssClass="btn btn-danger" OnClick="Button1_Click" />
        &nbsp; 
        <asp:Button ID="Button2" runat="server" Text="Finish" CssClass="btn btn-danger" OnClick="Button2_Click1" />
                &nbsp; 
        <asp:Button ID="Button3" runat="server" Text="Clear" CssClass="btn btn-danger" OnClick="Button3_Click" />
		</div>

    </form>

</asp:Content>
