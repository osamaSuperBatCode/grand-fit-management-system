﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace GrandFitManagementSystem
{
    public partial class Item : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection("server=WS141\\MSSQLSERVER2012;database=hrmdb;user id=hrmdbadmin;password=sqlserver");
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                itemDropDownList1.DataBind();    
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("INSERT INTO ITEM VALUES('" + itemTextBox1.Text + "')", con);
            cmd.ExecuteNonQuery();
            con.Close();
            itemTextBox1.Text = "";
            itemDropDownList1.DataBind();

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("DELETE FROM ITEM WHERE Item_Name='" + itemDropDownList1.Text + "'", con);
            cmd.ExecuteNonQuery();
            con.Close();
            itemTextBox1.Text = "";
            itemDropDownList1.DataBind();
        }

       
    }
}