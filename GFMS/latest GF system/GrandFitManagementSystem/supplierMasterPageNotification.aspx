﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="supplierMasterPageNotification.aspx.cs" Inherits="GrandFitManagementSystem.supplierMasterPageNotification" %>
<asp:Content ID="Content1" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        
        <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Customer <small>notification area</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
        <h2 class="text-left">New Supplier Has Been Added Successfully&nbsp; :)</h2>

        <h2 class="text-left">View Supplier Database</h2>
        <br />
        <asp:Button ID="Button1" runat="server" Text="Click Here" CssClass="btn btn-lg green" OnClick="Button1_Click" />
    </form>
</asp:Content>
