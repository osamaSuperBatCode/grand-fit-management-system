﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace GrandFitManagementSystem
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        string strcon = ConfigurationManager.ConnectionStrings["hrmdbConnectionString"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                BindGridData();

            }
        }

        private void BindGridData()
        {

            SqlConnection connection = new SqlConnection(strcon);

            SqlCommand command = new SqlCommand("SELECT imagename,ImageID from [Image]", connection);

            SqlDataAdapter daimages = new SqlDataAdapter(command);

            DataTable dt = new DataTable();

            daimages.Fill(dt);
                            
            gvImages.DataSource = dt;

            gvImages.DataBind();

            gvImages.Attributes.Add("bordercolor", "black");

        }

    }
}