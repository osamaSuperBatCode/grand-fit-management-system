﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="supplierUpload.aspx.cs" Inherits="GrandFitManagementSystem.supplierUpload" %>
<asp:Content ID="Content1" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Upload Fabric <small>sku and images</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="Supplier.aspx">
								New Fabric Order
							</a>
                            <i class="fa fa-angle-right"></i>
						</li>
                        <li>
							<a href="#">
								Upload Fabric Image
							</a>
							<i class="fa fa-angle-right"></i>
						</li>

						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
    <div>
        <h3>Choose the Image to Upload</h3>
        <hr />
        <asp:FileUpload ID="fileuploadimages" runat="server" />
            <br />
        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn red" onclick="btnSubmit_Click" />
    </div>
        <br />
        <h3>Uploaded SKUs</h3>
        <hr />
    <div>
        <asp:GridView runat="server" ID="gvImages" CssClass="table table-striped table-bordered table-hover" AutoGenerateColumns="False" DataSourceID="sqldataImages" >
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" />
                    <asp:BoundField DataField="ImageName" HeaderText="SKU" />
                    <asp:ImageField HeaderText="Image" DataImageUrlField="ImagePath" />
                </Columns>
        </asp:GridView>

        <asp:SqlDataSource ID="sqldataImages" runat="server"  ConnectionString="<%$ConnectionStrings:hrmdbConnectionString%>"
                SelectCommand="select * from ImagesPath" >
        </asp:SqlDataSource>
    </div>
        </form>

</asp:Content>
