﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;

namespace GrandFitManagementSystem
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void signInButton_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("server=WS141;database=hrmdb;user id=hrmdbadmin;password=sqlserver");
            SqlDataReader dr;
            SqlCommand cmd;

            if (Session["username"] != null && Session["type"] != null)
            {
                if (Session["username"] == "Client")
                {
                    Response.Redirect("gallery.aspx");
                }
                else if (Session["username"] == "Admin")
                {
                    Response.Redirect("clientMasterPage.aspx");
                }
            }

            if (loginSelect.Text == "Admin")
            {

                con.Open();
                cmd = new SqlCommand("select * from adminlogin", con);
                dr = cmd.ExecuteReader();


                while (dr.Read())
                {
                    if (txtUserName.Text == dr[1].ToString() && txtPassword.Text == dr[2].ToString())
                    {
                        Session["username"] = txtUserName.Text;
                        Session["type"] = "Admin";
                        Response.Redirect("clientMasterPage.aspx");
                    }
                    else
                    {
                        wrongUPLabel.Text = "Wrong Username or Password";
                        wrongUPLabel.Visible = true;
                    }

                }

            }
            else if (loginSelect.Text == "Client")
            {
                con.Open();
                cmd = new SqlCommand("select * from employeelogin", con);
                dr = cmd.ExecuteReader();


                while (dr.Read())
                {
                    if (txtUserName.Text == dr[1].ToString() && txtPassword.Text == dr[2].ToString())
                    {
                        Session["username"] = txtUserName.Text;
                        Session["type"] = "Employee";
                        Response.Redirect("gallery.aspx");
                    }
                    else
                    {
                        wrongUPLabel.Text = "Wrong Username or Password";
                        wrongUPLabel.Visible = true;
                    }

                }

            }

        }
    }
}