﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="GrandFitManagementSystem.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 542px;
        }
        .auto-style9 {
            width: 474px;
        }
        .auto-style11 {
            width: 1010px;
        }
        .auto-style13 {
            width: 704px;
        }
        .auto-style14 {
            width: 1080px;
        }
        .auto-style15 {
            width: 1128px;
        }
        .auto-style16 {
            width: 941px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

        <table class="auto-style1">
            <tr>
                <td class="auto-style2">Date</td>
                <td class="auto-style16">Item</td>
                <td class="auto-style15">Pattern</td>
                <td class="auto-style14">QuantityAvailable</td>
                <td class="auto-style11">Quantity</td>
                <td class="auto-style13">Customer Name</td>
                <td class="auto-style9">Amount</td>
                <td>Status</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:TextBox ID="DatetxtBox" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>
                </td>

                <td class="auto-style16">
                    <asp:DropDownList ID="itemddl" runat="server" DataSourceID="SqlDataSource1" DataTextField="Item_Name" DataValueField="Item_Name">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT * FROM [ITEM]"></asp:SqlDataSource>
                </td>

                <td class="auto-style14">
                    <asp:TextBox ID="txtBoxPattern" runat="server" CssClass="form-control"></asp:TextBox>
                </td>

                <td class="auto-style14">
                    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                </td>

                <td class="auto-style11">
                    <asp:TextBox ID="txtBoxQuantity" runat="server" CssClass="form-control" TextMode="Number"></asp:TextBox>
                </td>

                <td class="auto-style13">
                    <asp:TextBox ID="txtBoxCustomerName" runat="server" CssClass="form-control"></asp:TextBox>
                </td>

                <td class="auto-style9">
                    <asp:TextBox ID="txtBoxAmount" runat="server" CssClass="form-control"></asp:TextBox>
                </td>

                <td>
                    <asp:DropDownList ID="Statusddl" runat="server">
                        <asp:ListItem>Paid</asp:ListItem>
                        <asp:ListItem>Unpaid</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            
        </table>

        <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
    <div>
    
        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
    
        <br />
        <br />
        <table class="auto-style1">
            <tr>
                <td>item</td>
                <td>Pattern</td>
                <td>Quantity</td>
                <td>enter quantity</td>
                <td>Pattern ID</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tbqun" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
                </td>
                <td>
                    <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Button" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
