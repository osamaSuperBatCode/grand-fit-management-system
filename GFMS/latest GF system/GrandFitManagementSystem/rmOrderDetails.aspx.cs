﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace GrandFitManagementSystem
{

    public partial class rmOrderDetails : System.Web.UI.Page
    {
        

        int totalUnitPrice = 0;
        int totalQuantity = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                GridView1.DataBind();
            }

            SqlDataReader rdr;

            DataTable dt = new DataTable();
            
            //dt.Columns.Add("SalesId");
            dt.Columns.Add("SalesID");
            dt.Columns.Add("Date");
            dt.Columns.Add("ProductId");
            dt.Columns.Add("ProductValue");
            dt.Columns.Add("Quantity");
            
            
            

            DataRow dr;

            SqlConnection con = new SqlConnection("server=WS141\\MSSQLSERVER2012;database=hrmdb;user id=hrmdbadmin;password=sqlserver");
            SqlCommand cmd = new SqlCommand("SELECT * FROM ITEM_SALES", con);
            cmd.CommandType = CommandType.Text;

            using(con)
            {
                con.Open();
                rdr = cmd.ExecuteReader();

                while(rdr.Read())
                {
                    dr = dt.NewRow();
                    //dr["SalesId"] = rdr["ID_Sales"].ToString();
                    dr["ProductId"] = rdr["ID_Product"].ToString();
                    dr["ProductValue"] = rdr["Value_Product"].ToString();
                    dr["Quantity"] = rdr["Quantity"].ToString();

                    dt.Rows.Add(dr);
                    dt.AcceptChanges();

                }
            }

                    //datatable 2

                    SqlConnection conn = new SqlConnection("server=WS141\\MSSQLSERVER2012;database=hrmdb;user id=hrmdbadmin;password=sqlserver");
                    SqlCommand cmd2 = new SqlCommand("SELECT ID_Sales, Date_Sales FROM SALES", conn);
                    cmd2.CommandType = CommandType.Text;
                    
                    int i = 0;
                    using (conn)
                    {
                        conn.Open();
                        rdr = cmd2.ExecuteReader();


                        while (rdr.Read())
                        {

                            dt.Rows[i]["SalesID"] = rdr["ID_Sales"].ToString();
                            dt.Rows[i]["Date"] = rdr["Date_Sales"].ToString();
                            dr = dt.NewRow();

                            i++;
                            
                        }
                    }


                    GridView1.DataSource = dt;
                    GridView1.DataBind();

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            GridView1.AllowPaging = false;
            GridView1.DataBind();
            Response.ClearContent();
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "RM Purchase Records.xls"));
            Response.Charset = "";
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            //Change the Header Row back to white color
            GridView1.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            foreach (TableCell tableCell in GridView1.HeaderRow.Cells)
            {
                tableCell.Style["background-color"] = "#507CD1";
            }

            foreach(GridViewRow gridViewRow in GridView1.Rows)
            {
                gridViewRow.BackColor = System.Drawing.Color.White;
                foreach(TableCell gridViewRowTableCell in gridViewRow.Cells)
                {
                    gridViewRowTableCell.Style["background-color"] = "#EFF3FB";
                }
            }

           
            GridView1.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }
        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                totalUnitPrice += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Value_Product"));
                totalQuantity += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Quantity"));
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[3].Text = "Grand Total";
                e.Row.Cells[3].Font.Bold = true;

                e.Row.Cells[4].Text = totalUnitPrice.ToString();
                e.Row.Cells[4].Font.Bold = true;

                e.Row.Cells[5].Text = totalQuantity.ToString();
                e.Row.Cells[5].Font.Bold = true;
            }
        }


    }
}