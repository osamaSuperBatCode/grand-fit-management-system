﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="clientMasterPage.aspx.cs" Inherits="GrandFitManagementSystem.clientMasterPage" %>
<asp:Content ID="Content2" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
   
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Customer <small>details and more</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
                                Add New Customer
							</a>
                            <i class="fa fa-angle-right"></i>
						</li>
                        <li>
							<a href="clients.aspx">
                                Customer Database
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->

        <div>
            <h3>Personal Info</h3>
            <hr />

            <table class="nav-justified">
                <tr>
                    <td>
                        <h4>First Name</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbfir" runat="server" width="90%" CssClass="form-control" placeholder="First Name"></asp:TextBox>
                    </td>
                    <td>
                        <h4>Last Name</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tblas" runat="server" width="90%" CssClass="form-control" placeholder="Last Name"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4>Number</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbnum" runat="server" TextMode="Phone" width="90%" CssClass="form-control" placeholder="Number"></asp:TextBox>
                    </td>
                    <td>
                        <h4>Gender</h4>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlgen" runat="server" placeholder="Select" style="display: block;width: 90%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;">
                            <asp:ListItem>Male</asp:ListItem>
                            <asp:ListItem>Female</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4>Email</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbema" runat="server" width="90%" CssClass="form-control" placeholder="Email"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                </table>

        </div>
        <br />
        <div>
            <h3>Address and Other Info</h3>
            <hr />
            <table class="nav-justified">
                <tr>
                    <td>
                        <h4>About</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbabt" runat="server" width="90%" CssClass="form-control" placeholder="About"></asp:TextBox>
                    </td>
                    <td>
                        <h4>Registration Date</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbdat" runat="server" TextMode="Date" width="90%" CssClass="form-control" placeholder="Date"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4>Address</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbadd" runat="server" TextMode="MultiLine" width="90%" CssClass="form-control" placeholder="Address"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>

        </div>
        <br />
        <br />
        <div class="form action">
            <asp:Button ID="Button1" runat="server" Text="Create Client" CssClass="btn btn-lg yellow center-block" OnClick="Button1_Click1" />
        </div>  
    </form>
</asp:Content>