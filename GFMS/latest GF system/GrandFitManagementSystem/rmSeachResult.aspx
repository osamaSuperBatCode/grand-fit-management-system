﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="rmSeachResult.aspx.cs" Inherits="GrandFitManagementSystem.rmSeachResult" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Readymade Checkout <small>seach results</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="readymadeCheckout.aspx">
								Search Product
							</a>
                            <i class="fa fa-angle-right"></i>
						</li>
                        <li>
							<a href="#">
								Choose Product
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
    <form id="form1" runat="server">
        <div class="portlet box yellow">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Select Product
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
            <div class="portlet-body">

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [ID_Product], [Name_Product], [Value_Product] FROM [PRODUCT] WHERE ([Name_Product] LIKE '%' + @Name_Product + '%')">
        <SelectParameters>
            <asp:QueryStringParameter Name="Name_Product" QueryStringField="Search" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" CssClass="table-responsive table table-striped table-bordered table-hover" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="ID_Product" DataSourceID="SqlDataSource1" Width="631px">
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="ID_Product" DataNavigateUrlFormatString="cart1.aspx?ID={0}" DataTextField="NAME_PRODUCT" HeaderText="Results" />
            <asp:BoundField DataField="Name_Product" HeaderText="Products" SortExpression="Name_Product" />
            <asp:BoundField DataField="Value_Product" HeaderText="Product Value" SortExpression="Value_Product" />
        </Columns>
    </asp:GridView>
                </div>
            </div>
        </form>

</asp:Content>
