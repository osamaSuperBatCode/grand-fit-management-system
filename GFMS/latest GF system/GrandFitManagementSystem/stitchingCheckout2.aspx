﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="stitchingCheckout2.aspx.cs" Inherits="GrandFitManagementSystem.stitchingCheckout2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					GrandFit Invoice <small>purchase details</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="stitchingCheckout.aspx">
								Checkout Billing
							</a>
							<i class="fa fa-angle-right"></i>
                        
						</li>
                        <li>
							<a href="#">
								Stitching Invoice
							</a>
							<i class="fa fa-angle-right"></i>
                        
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->

            <!-- BEGIN PAGE CONTENT-->
    <form runat="server">
        				<div class="row invoice-logo">
					<div class="col-xs-6 invoice-logo-space">
						<img src="assets/img/invoice/walmart.png" alt=""/>
					</div>
					<div class="col-xs-6">
						<h3>
                            Shop 301, 7 Bunglows, Versova, 
							<span class="muted">
                                Andheri West, Mumbai - 400061
							</span>
						</h3>
                        <h4>
                            Tel: 9919786070 / 528491 
                        </h4>
                        <h4>
                            mehraaz@grandfit.com
                        </h4>
                        <h4><asp:Label ID="date" runat="server" Text="Label"></asp:Label></h4>
					</div>
				</div>
				<hr/>
				<div class="row">
					<div class="col-xs-4">
						<h3>Client:</h3>
                        
                        </div>
                <div class="col-xs-4">
						<h3>About Us:</h3>
                    </div>
                <div class="col-xs-4" >
						<h3>Payment Details:</h3>
                    </div>
                    </div>
        <hr />
        <div>


            <table class="table table-striped table-hover">
                <tr>
                    <td style="height: 18px">Item</td>
                    <td style="height: 18px">Pattern</td>
                    <td style="height: 18px">Quantity</td>
                    <td style="height: 18px">Unit Cost</td>
                    <td style="height: 18px">Total</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblite1" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblpat1" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblqua1" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lbluni1" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lbltot1" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblite2" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblpat2" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblqua2" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lbluni2" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lbltot2" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblite3" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblpat3" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblqua3" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lbluni3" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lbltot3" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
            </table>


        </div>
        <div>

            <table class="nav-justified">
                <tr>
                    <td style="width: 229px"><strong>Sub-Total Amount:</strong></td>
                    <td>
                        <asp:Label ID="lblsubtot" runat="server" Text="Label" style="font-weight: bold"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 229px"><strong>Discount:</strong></td>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server" style="font-weight: bold"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 229px; font-weight: bold"><b>GrandTotal:</b></td>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Label" style="font-weight: bold"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 229px; font-weight: bold"><b>Advance:</b></td>
                    <td>
                        <asp:TextBox ID="TextBox2" runat="server" style="font-weight: bold"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 229px; font-weight: bold"><b>Balance:</b></td>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
            </table>

        </div>

					<div class="col-xs-8 invoice-block">
						<br/>
						<a class="btn btn-lg blue hidden-print" onclick="javascript:window.print();">
							 Print <i class="fa fa-print"></i>
						</a>
						<a class="btn btn-lg green hidden-print">
							 Submit Your Invoice <i class="fa fa-check"></i>
						</a>
					</div>
    </form>

</asp:Content>
