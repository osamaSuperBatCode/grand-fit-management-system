﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="stitchingCheckout.aspx.cs" Inherits="GrandFitManagementSystem.stitchingCheckout" %>
<asp:Content ID="Content2" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Stitching Checkout <small>billing</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Checkout Billing
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <form id="form1" runat="server">
        <div>
       <h3>Order Details</h3>
        <hr />
        <table class="nav-justified">
                <tr>
                    <td>
                        <h4>Order Date</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbord" width="90%" CssClass="form-control" TextMode="Date" placeholder="enter date" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <h4>Customer Name</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbcus" width="90%" CssClass="form-control" placeholder="enter name" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4>Delivery Date</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbdel" width="90%" CssClass="form-control" TextMode="Date" placeholder="enter date" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <h4>Customer Contact</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbcuscon" width="90%" CssClass="form-control" placeholder="enter contact" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4>Description</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbdes" runat="server" Width="90%" CssClass="form-control" TextMode="MultiLine" placeholder="enter measurements..."></asp:TextBox>
                    <td>
                        <h4>Delivery Status</h4>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlsta" runat="server" style="display: block;width: 90%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;">
                            <asp:ListItem>Not Delivered</asp:ListItem>
                            <asp:ListItem>Delivered</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                
            </table>
        
    </div>
        <div>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [Item_Name] FROM [ITEM]"></asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [SKU] FROM [SUPTABLE]" ></asp:SqlDataSource>
            <br />
            <h3>Checkout Details</h3>
            <hr />
            <table class="table table-bordered table-hover table-striped table-condensed flip-content">
                <tr>
                    <td class="input-small" style="width: 150px; text-align: center">
                        <h5>Item</h5>
                    </td>
                    <td class="text-center" style="width: 150px">
                        <h5>Fabric SKU</h5>
                    </td>
                    <td class="text-center" style="width: 150px">
                        <h5>Fabric ID</h5>
                    </td>
                    <td class="text-center" style="width: 150px">
                        <h5>&nbsp;Quantity</h5>
&nbsp;</td>
                    <td class="text-center" style="width: 150px">
                        <h5>Enter Quantity</h5>
                    </td>
                    <td class="text-center" style="width: 150px">
                        <h5>Unit Cost</h5>
                    </td>
                    <td class="text-center" style="width: 150px">
                        <h5>Total</h5>
                    </td>
                    <td class="text-center" style="width: 150px">
                        <h5>Action</h5>
                    </td>
                </tr>
                <tr>
                    <td class="input-small" style="width: 150px">
                        <asp:DropDownList ID="ddlite1" runat="server" DataSourceID="SqlDataSource1" DataTextField="Item_Name" DataValueField="Item_Name" Height="16px" style="display: block;width: 100%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;">
                            <asp:ListItem>...</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 150px">
                        <asp:DropDownList ID="ddlpat1" runat="server" style="display: block;width: 100%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;" OnSelectedIndexChanged="ddlpat1_SelectedIndexChanged" AutoPostBack="True" >
                            <asp:ListItem>...</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 150px">
                        <h5 class="text-center">
            <asp:Label ID="lblnew1" runat="server" Text="..."></asp:Label>
                        </h5>
                    </td>
                    <td style="width: 150px">
                        <h5 class="text-center">
                        <asp:Label ID="lblqua1" runat="server" Text=""></asp:Label>
                        </h5>
                    </td>
                    <td class="input-xsmall" style="width: 150px">
                        <asp:TextBox ID="tbqua1" CssClass="form-control" placeholder="enter cms" runat="server"></asp:TextBox>
                    </td>
                    <td class="input-small" style="width: 150px">
                        <asp:TextBox ID="tbuni1" CssClass="form-control" placeholder="price cm" runat="server"></asp:TextBox>
                    </td>
                    <td class="input-small" style="width: 150px; text-align: center">
                        <h6>
                        <asp:Label ID="lbltot1" runat="server" Text="..."></asp:Label>
                        </h6>
                    </td>
                    <td class="input-small" style="width: 150px; text-align: center">
                        <asp:Button ID="btntot1" runat="server" Text="Calc" CssClass="btn btn-sm yellow" OnClick="btntot1_Click" />
                    &nbsp;<asp:Button ID="btnreg1" runat="server" Text="Register" CssClass="btn btn-sm red" OnClick="btnreg1_Click"/>
                    </td>
                </tr>
                <tr>
                    <td class="input-small" style="width: 150px">
                        <asp:DropDownList ID="ddlite2" runat="server" DataSourceID="SqlDataSource1" DataTextField="Item_Name" DataValueField="Item_Name" Height="16px" style="display: block;width: 100%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;">
                            <asp:ListItem>...</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 150px">
                        <asp:DropDownList ID="ddlpat2" runat="server" Height="16px" style="display: block;width: 100%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;" OnSelectedIndexChanged="ddlpat2_SelectedIndexChanged" AutoPostBack="True" >
                            <asp:ListItem>...</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 150px">
                        <h5 class="text-center"><asp:Label ID="lblnew2" runat="server" Text="..."></asp:Label>
                        </h5>
                    </td>
                    <td style="width: 150px">
                        <h5 class="text-center">
                        <asp:Label ID="lblqua2" runat="server" Text=""></asp:Label>
                        </h5>
                    </td>
                    <td class="input-xsmall" style="width: 150px">
                        <asp:TextBox ID="tbqua2" CssClass="form-control" placeholder="enter cms"  runat="server"></asp:TextBox>
                    </td>
                    <td class="input-small" style="width: 150px">
                        <asp:TextBox ID="tbuni2" CssClass="form-control" placeholder="price cm"  runat="server"></asp:TextBox>
                    </td>
                    <td class="input-small" style="width: 150px; text-align: center">
                        <h6>
                        <asp:Label ID="lbltot2" runat="server" Text="..."></asp:Label>
                        </h6>
                    </td>
                    <td class="input-small" style="width: 150px; text-align: center">
                        <asp:Button ID="btntot2" runat="server" Text="Calc" CssClass="btn btn-sm yellow" OnClick="btntot2_Click" />
                    &nbsp;<asp:Button ID="btnreg2" runat="server" Text="Register" CssClass="btn btn-sm red" OnClick="btnreg2_Click"/>
                    </td>
                </tr>
                <tr>
                    <td class="input-small" style="width: 150px">
                        <asp:DropDownList ID="ddlite3" runat="server" DataSourceID="SqlDataSource1" DataTextField="Item_Name" DataValueField="Item_Name" Height="16px" style="display: block;width: 100%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;">
                            <asp:ListItem>...</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 150px">
                        <asp:DropDownList ID="ddlpat3" runat="server" Height="16px" style="display: block;width: 100%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;" AutoPostBack="True" OnSelectedIndexChanged="ddlpat3_SelectedIndexChanged" >
                            <asp:ListItem>...</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 150px">
                        <h5 class="text-center"><asp:Label ID="lblnew3" runat="server" Text="..."></asp:Label>
                        </h5>
                    </td>
                    <td style="width: 150px">
                        <h5 class="text-center">
                        <asp:Label ID="lblqua3" runat="server" Text=""></asp:Label>
                        </h5>
                    </td>
                    <td class="input-xsmall" style="width: 150px">
                        <asp:TextBox ID="tbqua3" CssClass="form-control" placeholder="enter cms"  runat="server"></asp:TextBox>
                    </td>
                    <td class="input-small" style="width: 150px">
                        <asp:TextBox ID="tbuni3" CssClass="form-control" placeholder="price cm"  runat="server"></asp:TextBox>
                    </td>
                    <td class="input-small" style="width: 150px; text-align: center">
                        <h6>
                        <asp:Label ID="lbltot3" runat="server" Text="..."></asp:Label>
                        </h6>
                    </td>
                    <td class="input-small" style="width: 150px; text-align: center">
                        <asp:Button ID="btntot3" runat="server" Text="Calc" CssClass="btn btn-sm yellow" OnClick="btntot3_Click" />
                    &nbsp;<asp:Button ID="btnreg3" runat="server" Text="Register" CssClass="btn btn-sm red" OnClick="btnreg3_Click"/>
                    </td>
                </tr>
                <tr>
                    <td class="input-small" style="width: 150px">
                        <h5>
                            &nbsp;</h5>
                    </td>
                    <td style="width: 150px">
                        <h5>
                            &nbsp;</h5>
                    </td>
                    <td style="width: 150px">
                        <h4>
                        <asp:Label ID="lblrow1" runat="server" Font-Italic="True" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" ForeColor="#009ADE"></asp:Label>
                        </h4>
                    </td>
                    <td style="width: 150px">
                        <h4>
                        <asp:Label ID="lblrow2" runat="server" Font-Italic="True" Font-Underline="False" ForeColor="#00ABEF"></asp:Label>
                        </h4>
                    </td>
                    <td class="input-xsmall" style="width: 150px">
                        <h4>
                        <asp:Label ID="lblrow3" runat="server" Font-Italic="True" ForeColor="#009ADE"></asp:Label>
                        </h4>
                    </td>
                    <td class="input-small" style="width: 150px">
                        <h4>Grand Total</h4>
                    </td>
                    <td class="input-small" style="width: 150px; text-align: center">
                        <h4>
                        <asp:Label ID="lblgra" runat="server" Text=""></asp:Label>
                        </h4>
                    </td>
                    <td class="input-small" style="width: 150px; text-align: center">
                        <asp:Button ID="btngratot" runat="server" OnClick="btngratot_Click" Text="Total" CssClass="btn btn-lg green" />
                    </td>
                </tr>
                </table>

        </div>
        <br />
    
                
        <!--<div>
            &nbsp;&nbsp;&nbsp;<h3>Measurement Details</h3>
            <hr />
            <table class="table table-bordered table-striped table-condensed flip-content">
                <tr>
                    <td class="text-left">
                        <h5>Length</h5>
                    </td>
                    <td class="text-left">
                        <h5>Collar</h5>
                    </td>
                    <td class="text-left">
                        <h5>Shoulder</h5>
                    </td>
                    <td class="text-left">
                        <h5>Chest</h5>
                    </td>
                    <td class="text-left">
                        <h5>Waist</h5>
                    </td>
                    <td class="text-left">
                        <h5>Hip</h5>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="tblen" CssClass="form-control" placeholder="..." runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="tbcol" CssClass="form-control" placeholder="..." runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="tbsho" CssClass="form-control" placeholder="..." runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="tbche" CssClass="form-control" placeholder="..." runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="tbwai" CssClass="form-control" placeholder="..." runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="tbhip" CssClass="form-control" placeholder="..." runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>-->
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Invoice" CssClass="btn btn-lg blue-stripe center-block" OnClick="Button1_Click" />

    &nbsp;&nbsp;</form>
</asp:Content>