﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GrandFitManagementSystem
{
    public partial class readymadeProduct : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lbInsert_Click(object sender, EventArgs e)
        {
            SqlDataSource1.InsertParameters["Name_Product"].DefaultValue = 
                ((TextBox)GridView1.FooterRow.FindControl("txtName")).Text;
            SqlDataSource1.InsertParameters["Value_Product"].DefaultValue =
                ((TextBox)GridView1.FooterRow.FindControl("txtValue")).Text;

            SqlDataSource1.Insert();

        }
    }
}