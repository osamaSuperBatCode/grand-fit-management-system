﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="Pattern.aspx.cs" Inherits="GrandFitManagementSystem.Pattern" %>
<asp:Content ID="Content2" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
   
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Customer<small>details and more</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
    <form runat="server">
        <label class="control-label" style="font-size: medium">Items Available</label>
        <div class="form control">
            <asp:DropDownList ID="itemDropDownList2" runat="server" placeholder="Select" style="display: block;width: 30%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;" DataSourceID="SqlDataSource1" DataTextField="Item_Name" DataValueField="Item_Name">
                <asp:ListItem>Shirt</asp:ListItem>
                <asp:ListItem>Pant</asp:ListItem>
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [Item_Name] FROM [ITEM]"></asp:SqlDataSource>
        </div>
        </br>
        <label class="control-label" style="font-size: medium">Patterns Available</label>
        <div class="form control">
            <asp:DropDownList ID="patternDropDownList1" runat="server" placeholder="Select" style="display: block;width: 30%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;" DataSourceID="SqlDataSource2" DataTextField="Pattern_Name" DataValueField="Pattern_Name">
                <asp:ListItem>Long</asp:ListItem>
                <asp:ListItem>Short</asp:ListItem>
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [Pattern_Name] FROM [PATTERN]"></asp:SqlDataSource>
        </div>
        </br>
        <div class="form-group">
			<label class="control-label" style="font-size: medium">New Pattern Name</label>
                <asp:TextBox ID="patternTextBox1" runat="server" placeholder="Name" style="display: block;width: 30%;font-size: 14px;height: 34px;padding: 2px 12px;"></asp:TextBox>
		    <br />
            Quantity<br />
            <asp:TextBox ID="quantityTextBox1" runat="server"></asp:TextBox>
		</div>
        <div class="">
                <asp:Button ID="Button1" runat="server" Text="Add Pattern" CssClass="btn btn-danger" OnClick="Button1_Click" />
		    </div>

        </form>
</asp:Content>
