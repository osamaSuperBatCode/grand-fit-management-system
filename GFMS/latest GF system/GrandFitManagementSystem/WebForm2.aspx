﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="GrandFitManagementSystem.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">

<div>

<asp:FileUpload ID="fileuploadimages" runat="server" />

<br />

<asp:Button ID="btnSubmit" runat="server" Text="Submit" onclick="btnSubmit_Click" />

</div>

<div>

<asp:GridView runat="server" ID="gvImages" AutoGenerateColumns="false" DataSourceID="sqldataImages" CssClass="Gridview" HeaderStyle-BackColor="#61A6F8" >

<Columns>

<asp:BoundField DataField="ID" HeaderText="ID" />

<asp:BoundField DataField="ImageName" HeaderText="Image Name" />

<asp:ImageField HeaderText="Image" DataImageUrlField="ImagePath" />

</Columns>

</asp:GridView>

<asp:SqlDataSource ID="sqldataImages" runat="server"  ConnectionString="<%$ConnectionStrings:hrmdbConnectionString%>"

SelectCommand="select * from ImagesPath" >

</asp:SqlDataSource>

</div>

</form>
</asp:Content>
