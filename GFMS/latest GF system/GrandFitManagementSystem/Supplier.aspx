﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="Supplier.aspx.cs" Inherits="GrandFitManagementSystem.Supplier" %>
<asp:Content ID="Content2" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Stitching Fabric <small>supplier details and more</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
                        <li>
							<a href="#">
								New Fabric Order
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
                        <li>
							<a href="supplierUpload.aspx">
								Upload Fabric Image
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="suppliers.aspx">
								View Orders
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
<form id="form1" runat="server">

    <div>
        <h3>General Information</h3>
        <hr />
        <table class="nav-justified">
            <tr>
                <td>
                    <h4>Supplier Name</h4>
                </td>
                <td>
                    <asp:TextBox ID="tbsup" runat="server" width="90%" CssClass="form-control" placeholder="company name"></asp:TextBox>
                </td>
                <td>
                    <h4>Submitted By</h4>
                </td>
                <td>
                    <asp:TextBox ID="tbsub" runat="server" width="90%" CssClass="form-control" placeholder="person name"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Fabric SKU</h4>
                </td>
                <td>
                    <asp:TextBox ID="tbsku" runat="server" width="90%" CssClass="form-control" placeholder="#sku"></asp:TextBox>
                </td>
                <td>
                    <h4>Quantity</h4>
                </td>
                <td>
                    <asp:TextBox ID="tbqua" runat="server" width="90%" CssClass="form-control" placeholder="enter cms"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Fabric Price</h4>
                </td>
                <td>
                    <asp:TextBox ID="tbfab" runat="server" width="90%" CssClass="form-control" placeholder="per cm"></asp:TextBox>
                </td>
            </tr>
        </table>

    </div>
    <br />
    <div>
        <h3>Other Details</h3>
        <hr />
        <table class="nav-justified">
            <tr>
                <td>
                    <h4>Date</h4>
                </td>
                <td>
                    <asp:TextBox ID="tbdat" TextMode="Date" runat="server" width="90%" CssClass="form-control" placeholder="mm/dd/yy"></asp:TextBox>
                </td>
                <td>
                    <h4>Approved By</h4>
                </td>
                <td>
                    <asp:TextBox ID="tbapp" runat="server" width="90%" CssClass="form-control" placeholder="person name"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Cost</h4>
                </td>
                <td>
                    <asp:TextBox ID="tbcos" runat="server" width="90%" CssClass="form-control" placeholder="price"></asp:TextBox>
                </td>
                
            </tr>
        </table>

    </div>
    <br />
    
    <br />
    <br />
    <div class="form action">
            <asp:Button ID="btnreg" runat="server" Text="Register New Order" CssClass="btn btn-lg yellow center-block" OnClick="Button1_Click1" />
        </div> 
</form>
</asp:Content>
