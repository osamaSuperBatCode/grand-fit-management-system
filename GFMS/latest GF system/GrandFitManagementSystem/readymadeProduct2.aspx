﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="readymadeProduct2.aspx.cs" Inherits="GrandFitManagementSystem.readymadeProduct2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Readymade Products List <small>products</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="readymadeProduct.aspx">
								Add Products
							</a>
                            <i class="fa fa-angle-right"></i>
						</li>
                        <li>
							<a href="#">
								View Products
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
    <form id="form1" runat="server">
        <hr />
        <div>
            <asp:TextBox ID="tbsearch"  runat="server" width="70%" CssClass="form-control" placeholder="search for products"></asp:TextBox>
            <br />
                <asp:Button ID="Button1" CssClass="btn red"  runat="server" Text="Search Columns" OnClick="Button1_Click"  />    
        </div>
        <br />
        <div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Readymade Products Database
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
    <asp:GridView ID="GridView1" runat="server" CssClass="table-responsive table table-striped table-bordered table-hover" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="ID_Product" DataSourceID="SqlDataSource1" >
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="ID_Product" HeaderText="Product ID" InsertVisible="False" ReadOnly="True" SortExpression="ID_Product" />
            <asp:BoundField DataField="Name_Product" HeaderText="Product Name" SortExpression="Name_Product" />
            <asp:BoundField DataField="Value_Product" HeaderText="Product Price" SortExpression="Value_Product" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" DeleteCommand="DELETE FROM [PRODUCT] WHERE [ID_Product] = @original_ID_Product AND (([Name_Product] = @original_Name_Product) OR ([Name_Product] IS NULL AND @original_Name_Product IS NULL)) AND (([Value_Product] = @original_Value_Product) OR ([Value_Product] IS NULL AND @original_Value_Product IS NULL))" InsertCommand="INSERT INTO [PRODUCT] ([Name_Product], [Value_Product]) VALUES (@Name_Product, @Value_Product)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [PRODUCT]" UpdateCommand="UPDATE [PRODUCT] SET [Name_Product] = @Name_Product, [Value_Product] = @Value_Product WHERE [ID_Product] = @original_ID_Product AND (([Name_Product] = @original_Name_Product) OR ([Name_Product] IS NULL AND @original_Name_Product IS NULL)) AND (([Value_Product] = @original_Value_Product) OR ([Value_Product] IS NULL AND @original_Value_Product IS NULL))">
        <DeleteParameters>
            <asp:Parameter Name="original_ID_Product" Type="Int32" />
            <asp:Parameter Name="original_Name_Product" Type="String" />
            <asp:Parameter Name="original_Value_Product" Type="Decimal" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Name_Product" Type="String" />
            <asp:Parameter Name="Value_Product" Type="Decimal" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name_Product" Type="String" />
            <asp:Parameter Name="Value_Product" Type="Decimal" />
            <asp:Parameter Name="original_ID_Product" Type="Int32" />
            <asp:Parameter Name="original_Name_Product" Type="String" />
            <asp:Parameter Name="original_Value_Product" Type="Decimal" />
        </UpdateParameters>
    </asp:SqlDataSource>
                </div>
            </div>
            </div>
        <div class="portlet box yellow">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Serach Results
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
                            <asp:GridView ID="GridView2" CssClass="table table-striped table-bordered table-hover" runat="server"></asp:GridView>
                            </div>
            </div>
       
                </form>
</asp:Content>
