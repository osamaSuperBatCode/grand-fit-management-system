﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;


namespace GrandFitManagementSystem
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection("server=WS141\\MSSQLSERVER2012;database=hrmdb;user id=hrmdbadmin;password=sqlserver");
        protected void Page_Load(object sender, EventArgs e)
        {

            /*SqlCommand command = new SqlCommand();
            SqlDataReader reader;
            command.Connection = con;

            string s = txtBoxPattern.Text;
            int q = Convert.ToInt32(txtBoxQuantity.Text);
            command.CommandText = "SELECT QUANTITY FROM PATTERN WHERE Id = s";

            con.Open();
            reader = command.ExecuteReader();

            while( reader.Read() )
            {
                string quantity = reader["QUANTITY"].ToString();

                Label1.Text = quantity;
            }

            reader.Close();
            con.Close();*/

            /*try
            {
                SqlDataReader myReader = null;
                SqlCommand myCommand = new SqlCommand("SELECT QUANTITY FROM PATTERN", con);

                myReader = myCommand.ExecuteReader();
                while (myReader.Read())
                {
                    Label1.Text = myReader["Quantity"].ToString();
                }
            }
            catch(Exception ex)

            {
                Label2.Text = "Error";
            }*/
            


            if(!IsPostBack)
            {
                SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM PATTERN", con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DropDownList2.DataSource = dt;
                DropDownList2.DataTextField = "Pattern_Name";
                DataBind();
            }


        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            
            /*con.Open();
            string str = "INSERT INTO STITCHINGCHECKOUT (Date, Item, Pattern, Subtotal, Quantity, CustomerName, Amount, Status) values('" + DatetxtBox .Text+ "','" + itemddl.Text + "','" + txtBoxPattern.Text + "','" + txtBoxQuantity.Text + "','" + txtBoxCustomerName.Text + "','" + txtBoxAmount.Text + "','" + Statusddl.Text + "')";

            //string aSQL = "UPDATE PATTERN SET QUANTITY = QUANTITY - q " + "WHERE Id = '"& id &"';
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            con.Close();*/

            SqlCommand cmd2 = new SqlCommand("UPDATE PATTERN SET Quantity='" + Label3.Text + "' WHERE Pattern_Name= '" + DropDownList2.Text + "'",con);
            cmd2.ExecuteNonQuery();
            con.Close();

            
            
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM PATTERN WHERE Pattern_Name='" + DropDownList2.Text + "'", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            Label3.Text = dt.Rows[0][1].ToString();
            Label4.Text = dt.Rows[0][2].ToString();

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            int qunty = Convert.ToInt32(tbqun.Text);
            int stock = Convert.ToInt32(Label3.Text);
            int chgstock = stock - qunty;

            con.Open();

            SqlCommand cmd1 = new SqlCommand("UPDATE PATTERN SET Quantity=@quantity WHERE Id=@Id", con);
            cmd1.Parameters.AddWithValue("@quantity", chgstock);
            cmd1.Parameters.AddWithValue("@Id", Label4.Text);

            cmd1.ExecuteNonQuery();
            con.Close();

            
        }
    }
}