﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="employeeMasterPage.aspx.cs" Inherits="GrandFitManagementSystem.employeeMasterPage" %>
<asp:Content ID="Content2" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Employee <small>details and more</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Add New Employee
							</a>
                            <i class="fa fa-angle-right"></i>
						</li>
                        <li>
							<a href="#">
								Employee information
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
    <div>
        <h3>Personal Information</h3>
        <hr />

        <table class="nav-justified">
            <tr>
                <td>
                    <h4>First Name</h4>
                </td>
                <td>
                    <asp:TextBox ID="tbfir" runat="server" CssClass="form-control" width="90%" placeholder="First Name"></asp:TextBox>
                </td>
                <td>
                    <h4>Last Name</h4>
                </td>
                <td>
                    <asp:TextBox ID="tblas" runat="server" CssClass="form-control" width="90%" placeholder="Last Name"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Number</h4>
                </td>
                <td>
                    <asp:TextBox ID="tbnum" runat="server" TextMode="Phone" width="90%" CssClass="form-control" placeholder="Number"></asp:TextBox>
                </td>
                <td>
                    <h4>Gender</h4>
                </td>
                <td>
                    <asp:DropDownList ID="ddlgen" runat="server" placeholder="Select" style="display: block;width: 90%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;">
                        <asp:ListItem>Male</asp:ListItem>
                        <asp:ListItem>Female</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Date of Birth</h4>
                </td>
                <td>
                    <asp:TextBox ID="tbdob" runat="server" TextMode="Date" width="90%" CssClass="form-control" placeholder="dob"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>

    </div>
        <br />
        <div>
            <h3>Remark and Address</h3>
            <hr />
            <table class="nav-justified">
                <tr>
                    <td>
                        <h4>Category</h4>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlcat" runat="server" style="display: block;width: 90%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;">
                            <asp:ListItem>Monthly</asp:ListItem>
                            <asp:ListItem>Weekly</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <h4>Remark</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbrem" runat="server" width="90%" CssClass="form-control" placeholder="..."></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4>Address 1</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbadd1" runat="server" TextMode="MultiLine" width="90%" CssClass="form-control" placeholder="Address 1"></asp:TextBox>
                    </td>
                    <td>
                        <h4>Address 2</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbadd2" runat="server" TextMode="MultiLine" width="90%" CssClass="form-control" placeholder="Address 2"></asp:TextBox>
                    </td>
                </tr>
                </table>

        </div>
        <br />
        <br />
        <div class="">
            <asp:Button ID="Button1" runat="server" Text="Create Employee" CssClass="btn btn-lg yellow center-block" OnClick="Button1_Click" />
        </div>
			<!-- END PAGE CONTENT-->
    </form>
</asp:Content>
