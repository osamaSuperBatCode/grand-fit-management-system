﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GrandFitManagementSystem
{
    public partial class readymadeCheckout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //can't search without typing
            if( searchProduct.Text != "" )
            {
                Response.Redirect("rmSeachResult.aspx?Search=" + searchProduct.Text);  
            }
        }
    }
}