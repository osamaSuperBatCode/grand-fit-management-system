﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="readymadeProduct.aspx.cs" Inherits="GrandFitManagementSystem.readymadeProduct" %>
<asp:Content ID="Content2" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Readymade Products List <small>add products</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Add Products
							</a>
                            <i class="fa fa-angle-right"></i>
						</li>
                        <li>
							<a href="readymadeProduct2.aspx">
								View Products
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
    <form id="form1" runat="server">
        <hr />
        <div class="portlet box yellow">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>ReadyMade Products Table
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
            <div class="portlet-body">
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" DeleteCommand="DELETE FROM [PRODUCT] WHERE [ID_Product] = @original_ID_Product AND (([Name_Product] = @original_Name_Product) OR ([Name_Product] IS NULL AND @original_Name_Product IS NULL)) AND (([Value_Product] = @original_Value_Product) OR ([Value_Product] IS NULL AND @original_Value_Product IS NULL))" InsertCommand="INSERT INTO [PRODUCT] ([Name_Product], [Value_Product]) VALUES (@Name_Product, @Value_Product)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT [ID_Product], [Name_Product], [Value_Product] FROM [PRODUCT]" UpdateCommand="UPDATE [PRODUCT] SET [Name_Product] = @Name_Product, [Value_Product] = @Value_Product WHERE [ID_Product] = @original_ID_Product AND (([Name_Product] = @original_Name_Product) OR ([Name_Product] IS NULL AND @original_Name_Product IS NULL)) AND (([Value_Product] = @original_Value_Product) OR ([Value_Product] IS NULL AND @original_Value_Product IS NULL))">
                    <DeleteParameters>
                        <asp:Parameter Name="original_ID_Product" Type="Int32" />
                        <asp:Parameter Name="original_Name_Product" Type="String" />
                        <asp:Parameter Name="original_Value_Product" Type="Decimal" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Name_Product" Type="String" />
                        <asp:Parameter Name="Value_Product" Type="Decimal" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Name_Product" Type="String" />
                        <asp:Parameter Name="Value_Product" Type="Decimal" />
                        <asp:Parameter Name="original_ID_Product" Type="Int32" />
                        <asp:Parameter Name="original_Name_Product" Type="String" />
                        <asp:Parameter Name="original_Value_Product" Type="Decimal" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:GridView ID="GridView1" runat="server" CssClass="table-responsive table table-striped table-bordered table-hover" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="ID_Product" DataSourceID="SqlDataSource1" ShowFooter="True" >
                    <Columns>
                        <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                        <asp:TemplateField HeaderText="Product ID" InsertVisible="False" SortExpression="ID_Product">
                            <EditItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("ID_Product") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID_Product") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:LinkButton onClick="lbInsert_Click" ID="lbInsert" runat="server">Insert</asp:LinkButton>
                            </FooterTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Product Name" SortExpression="Name_Product">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Name_Product") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Name_Product") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                            </FooterTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Product Value" SortExpression="Value_Product">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Value_Product") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("Value_Product") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtValue" runat="server"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                </div>
            </div>
        
</form>
</asp:Content>
