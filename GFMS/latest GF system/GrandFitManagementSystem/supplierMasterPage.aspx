﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="supplierMasterPage.aspx.cs" Inherits="GrandFitManagementSystem.supplierMasterPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Supplier <small>details and more</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Add New Supplier
							</a>
                            <i class="fa fa-angle-right"></i>
						</li>
                        <li>
							<a href="SupplierMasterPage2.aspx">
								Supplier Database
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->

        <div>
            <h3>Personal Info</h3>
            <hr />

            <table class="nav-justified">
                <tr>
                    <td>
                        <h4>Company Name</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbcom" runat="server" width="90%" CssClass="form-control" placeholder="comapny name"></asp:TextBox>
                    </td>
                    <td>
                        <h4>Email</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbema" runat="server" width="90%" CssClass="form-control" placeholder="Last Name"></asp:TextBox>
                    </td>
                </tr>
               
                <tr>
                    <td>
                        <h4>Description</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbdes" runat="server" TextMode="MultiLine" width="90%" CssClass="form-control" placeholder="Email"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                </table>

        </div>
        <br />
        <div>
            <h3>Address and Other Info</h3>
            <hr />
            <table class="nav-justified">
                <tr>
                    <td>
                        <h4>Contact 1</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbcon1" runat="server" width="90%" CssClass="form-control" placeholder="contact number 1"></asp:TextBox>
                    </td>
                    <td>
                        <h4>Contact 2</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbcon2" runat="server" width="90%" CssClass="form-control" placeholder="contact number 2"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4>Address</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbadd" runat="server" TextMode="MultiLine" width="90%" CssClass="form-control" placeholder="Address"></asp:TextBox>
                    </td>
                    <td>
                        <h4>About</h4>
                    </td>
                    <td>
                        <asp:TextBox ID="tbabt" runat="server" TextMode="MultiLine" width="90%" CssClass="form-control" placeholder="About"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>

        </div>
        <br />
        <br />
        <div class="form action">
            <asp:Button ID="Button1" runat="server" Text="Create Supplier" CssClass="btn btn-lg yellow center-block" OnClick="Button1_Click" />
        </div>  
    </form>
</asp:Content>
