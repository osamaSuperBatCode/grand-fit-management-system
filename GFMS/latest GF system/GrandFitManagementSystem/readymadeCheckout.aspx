﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="readymadeCheckout.aspx.cs" Inherits="GrandFitManagementSystem.readymadeCheckout" %>
<asp:Content ID="Content2" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Readymade Checkout <small>search products</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Search Product
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->

    <form id="form1" runat="server">
        <h3>Search For Products</h3>
        <div class="input-group">
            <asp:TextBox ID="searchProduct" runat="server" placeholder="enter product name" style="display: block;width: 100%;font-size: 14px;height: 34px;padding: 2px 12px;"></asp:TextBox>
            <span class="input-group-btn">
                <asp:Button ID="Button1" runat="server" Text="Search" CssClass="btn red" OnClick="Button1_Click" />
		    </span>
        </div>
								
        <br />
        <br />
         
        <div class="portlet box yellow">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Readymade Products Table
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
            <div class="portlet-body">

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [ID_Product], [Name_Product], [Value_Product] FROM [PRODUCT]">
        </asp:SqlDataSource>
        <asp:GridView ID="GridView1" runat="server" CssClass="table-responsive table table-striped table-bordered table-hover" AutoGenerateColumns="False" DataKeyNames="ID_Product" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:BoundField DataField="ID_Product" HeaderText="Product ID" InsertVisible="False" ReadOnly="True" SortExpression="ID_Product" />
                <asp:BoundField DataField="Name_Product" HeaderText="Product Name" SortExpression="Name_Product" />
                <asp:BoundField DataField="Value_Product" HeaderText="Product Price" SortExpression="Value_Product" />
            </Columns>
        </asp:GridView>
                </div>
            </div>
     </form>
        
</asp:Content>
