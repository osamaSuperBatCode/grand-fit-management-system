﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="cart1.aspx.cs" Inherits="GrandFitManagementSystem.cart1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Readymade Checkout <small>check quantity</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="readymadeCheckout.aspx">
								Search Product
							</a>
                            <i class="fa fa-angle-right"></i>
						</li>
                        <li>
							<a href="rmSeachResult.aspx">
								Choose Product
							</a>
                            <i class="fa fa-angle-right"></i>
						</li>
                        <li>
							<a href="#">
								Set Details
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
    <form id="form1" runat="server">
        <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID_Product" DataSourceID="SqlDataSource1" Width="347px">
            <EditItemTemplate>
                ID_Product:
                <asp:Label ID="ID_ProductLabel1" runat="server" Text='<%# Eval("ID_Product") %>' />
                <br />
                Name_Product:
                <asp:TextBox ID="Name_ProductTextBox" runat="server" Text='<%# Bind("Name_Product") %>' />
                <br />
                Value_Product:
                <asp:TextBox ID="Value_ProductTextBox" runat="server" Text='<%# Bind("Value_Product") %>' />
                <br />
                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
            </EditItemTemplate>
            <InsertItemTemplate>
                Name_Product:
                <asp:TextBox ID="Name_ProductTextBox" runat="server" Text='<%# Bind("Name_Product") %>' />
                <br />
                Value_Product:
                <asp:TextBox ID="Value_ProductTextBox" runat="server" Text='<%# Bind("Value_Product") %>' />
                <br />
                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
                &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
            </InsertItemTemplate>
            <ItemTemplate>
                <h3><span>Product Name : </span>
                    <asp:Label ID="Name_ProductLabel" runat="server" CssClass="control-label" Text='<%# Bind("Name_Product") %>' />
                </h3>
                <h3>Product Value :
                    <asp:Label ID="Value_ProductLabel" runat="server" CssClass="control-label" Text='<%# Bind("Value_Product") %>' />
                </h3>
                <h3>Set Quantity</h3>
                <asp:TextBox ID="TextBox1" CssClass="form-control" runat="server"></asp:TextBox>
                <br />
                <asp:Button ID="Button1" runat="server" CssClass="btn red" OnClick="Button1_Click" Text="Add To Cart" />
                <br />
            </ItemTemplate>
        </asp:FormView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [ID_Product], [Name_Product], [Value_Product] FROM [PRODUCT] WHERE ([ID_Product] = @ID_Product)">
            <SelectParameters>
                <asp:QueryStringParameter Name="ID_Product" QueryStringField="ID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
