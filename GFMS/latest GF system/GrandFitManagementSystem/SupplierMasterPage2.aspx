﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="SupplierMasterPage2.aspx.cs" Inherits="GrandFitManagementSystem.SupplierMasterPage2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Supplier <small>Info and database</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="supplierMasterPage.aspx">
								Add New Supplier
							</a>
                            <i class="fa fa-angle-right"></i>
						</li>
                        <li>
							<a href="#">
								Supplier Database
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
    <form runat="server">
        <div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Supplier Information
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							
							
    <asp:GridView ID="GridView1" runat="server" CssClass="table table-striped table-bordered table-hover" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="SqlDataSource1" >
        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
            <asp:BoundField DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName" />
            <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
        </Columns>
    </asp:GridView>
                    <asp:Button ID="Button1" runat="server" Text="Add Client" CssClass="btn red center-block" />
                    
                </div>
            </div>

                <div class="portlet box yellow">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>More Details
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
        <asp:GridView ID="GridView2" runat="server" CssClass="table table-striped table-bordered table-hover" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" DataKeyNames="ID">
            <Columns>
                <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" InsertVisible="False" ReadOnly="True" />
                <asp:BoundField DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName" />
                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                <asp:BoundField DataField="Contact1" HeaderText="Contact1" SortExpression="Contact1" />
                <asp:BoundField DataField="Contact2" HeaderText="Contact2" SortExpression="Contact2" />
                <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                <asp:BoundField DataField="About" HeaderText="About" SortExpression="About" />
            </Columns>
        </asp:GridView>
                                </div>
                            </div>
                    </div>
        <div class="form-actions">                 
            <asp:Button ID="Button2" runat="server" Text="Export to Word" CssClass="btn red center-block" />
        </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [ID], [CompanyName], [Description] FROM [SUPPLIERTABLE]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" DeleteCommand="DELETE FROM [SUPPLIERTABLE] WHERE [ID] = @original_ID AND (([CompanyName] = @original_CompanyName) OR ([CompanyName] IS NULL AND @original_CompanyName IS NULL)) AND (([Email] = @original_Email) OR ([Email] IS NULL AND @original_Email IS NULL)) AND (([Description] = @original_Description) OR ([Description] IS NULL AND @original_Description IS NULL)) AND (([Contact1] = @original_Contact1) OR ([Contact1] IS NULL AND @original_Contact1 IS NULL)) AND (([Contact2] = @original_Contact2) OR ([Contact2] IS NULL AND @original_Contact2 IS NULL)) AND (([Address] = @original_Address) OR ([Address] IS NULL AND @original_Address IS NULL)) AND (([About] = @original_About) OR ([About] IS NULL AND @original_About IS NULL))" InsertCommand="INSERT INTO [SUPPLIERTABLE] ([CompanyName], [Email], [Description], [Contact1], [Contact2], [Address], [About]) VALUES (@CompanyName, @Email, @Description, @Contact1, @Contact2, @Address, @About)" SelectCommand="SELECT * FROM [SUPPLIERTABLE] WHERE ([ID] = @ID)" UpdateCommand="UPDATE [SUPPLIERTABLE] SET [CompanyName] = @CompanyName, [Email] = @Email, [Description] = @Description, [Contact1] = @Contact1, [Contact2] = @Contact2, [Address] = @Address, [About] = @About WHERE [ID] = @original_ID AND (([CompanyName] = @original_CompanyName) OR ([CompanyName] IS NULL AND @original_CompanyName IS NULL)) AND (([Email] = @original_Email) OR ([Email] IS NULL AND @original_Email IS NULL)) AND (([Description] = @original_Description) OR ([Description] IS NULL AND @original_Description IS NULL)) AND (([Contact1] = @original_Contact1) OR ([Contact1] IS NULL AND @original_Contact1 IS NULL)) AND (([Contact2] = @original_Contact2) OR ([Contact2] IS NULL AND @original_Contact2 IS NULL)) AND (([Address] = @original_Address) OR ([Address] IS NULL AND @original_Address IS NULL)) AND (([About] = @original_About) OR ([About] IS NULL AND @original_About IS NULL))" ConflictDetection="CompareAllValues" OldValuesParameterFormatString="original_{0}" >
            <DeleteParameters>
                <asp:Parameter Name="original_ID" Type="Int32" />
                <asp:Parameter Name="original_CompanyName" Type="String" />
                <asp:Parameter Name="original_Email" Type="String" />
                <asp:Parameter Name="original_Description" Type="String" />
                <asp:Parameter Name="original_Contact1" Type="Int32" />
                <asp:Parameter Name="original_Contact2" Type="Int32" />
                <asp:Parameter Name="original_Address" Type="String" />
                <asp:Parameter Name="original_About" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="CompanyName" Type="String" />
                <asp:Parameter Name="Email" Type="String" />
                <asp:Parameter Name="Description" Type="String" />
                <asp:Parameter Name="Contact1" Type="Int32" />
                <asp:Parameter Name="Contact2" Type="Int32" />
                <asp:Parameter Name="Address" Type="String" />
                <asp:Parameter Name="About" Type="String" />
            </InsertParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="GridView1" Name="ID" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="CompanyName" Type="String" />
                <asp:Parameter Name="Email" Type="String" />
                <asp:Parameter Name="Description" Type="String" />
                <asp:Parameter Name="Contact1" Type="Int32" />
                <asp:Parameter Name="Contact2" Type="Int32" />
                <asp:Parameter Name="Address" Type="String" />
                <asp:Parameter Name="About" Type="String" />
                <asp:Parameter Name="original_ID" Type="Int32" />
                <asp:Parameter Name="original_CompanyName" Type="String" />
                <asp:Parameter Name="original_Email" Type="String" />
                <asp:Parameter Name="original_Description" Type="String" />
                <asp:Parameter Name="original_Contact1" Type="Int32" />
                <asp:Parameter Name="original_Contact2" Type="Int32" />
                <asp:Parameter Name="original_Address" Type="String" />
                <asp:Parameter Name="original_About" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
</form>
</asp:Content>
