﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="Fabric.aspx.cs" Inherits="GrandFitManagementSystem.Fabric" %>
<asp:Content ID="Content1" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Stitching Fabric <small>supplier details and more</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
<form id="form1" runat="server">

    <div>
        <h3>Add New Fabric</h3>
        <hr />
        <table class="nav-justified">
            <tr>
                <td>
                    <h4>Supplier Name</h4>
                </td>
                <td>
                    <asp:TextBox ID="tbsup" runat="server" width="90%" CssClass="form-control" placeholder="company name"></asp:TextBox>
                </td>
                <td>
                    <h4>Fabric SKU</h4>
                </td>
                <td>
                    <asp:TextBox ID="tbfab" runat="server" width="90%" CssClass="form-control" placeholder="#sku"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Quantity</h4>
                </td>
                <td>
                    <asp:TextBox ID="tbqua" runat="server" width="90%" CssClass="form-control" placeholder="enter cms"></asp:TextBox>
                </td>
                <td>
                    <h4>Price</h4>
                </td>
                <td>
                    <asp:TextBox ID="tbpri" runat="server" width="90%" CssClass="form-control" placeholder="per cms"></asp:TextBox>
                </td>
            </tr>
        </table>

    </div>
    <br />
    <br />

    <div class="form action">
            <asp:Button ID="Button1" runat="server" Text="Add Fabric" CssClass="btn btn-lg red" OnClick="Button1_Click" />
        </div>
    </form>
</asp:Content>
