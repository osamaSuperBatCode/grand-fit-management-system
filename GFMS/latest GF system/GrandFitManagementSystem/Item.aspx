﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="Item.aspx.cs" Inherits="GrandFitManagementSystem.Item" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
        <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Stitching Items <small>add new items</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Add Fabric Item
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
        <hr />
        <h3>Add New Stitching Item</h3>
        <hr />
        <label class="control-label" style="font-size: medium">Fabric Items Available</label>
        <div class="form control">
            <asp:DropDownList ID="itemDropDownList1" runat="server" placeholder="Select" style="display: block;width: 30%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;" DataSourceID="SqlDataSource1" DataTextField="Item_Name" DataValueField="Item_Name">
                
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [Item_Name] FROM [ITEM]"></asp:SqlDataSource>
        </div>
        <br>
        <div class="form-group">
			<label class="control-label" style="font-size: medium">Enter New Item Name</label>
                <asp:TextBox ID="itemTextBox1" runat="server" width="30%" CssClass="form-control" placeholder="enter new name"></asp:TextBox>
		</div>
        <div class="">
		    </div>

        <div>
            <asp:Button ID="Button1" runat="server" CssClass="btn green" Text="Add New Item" OnClick="Button1_Click" />
&nbsp;<asp:Button ID="Button2" runat="server" CssClass="btn red" Text="Delete Item" OnClick="Button2_Click"  />
        </div>

        </form>
</asp:Content>
