﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="clients.aspx.cs" Inherits="GrandFitManagementSystem.clients" %>
<asp:Content ID="Content2" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Customer <small>info and database</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="clientMasterPage.aspx">
                                Add New Customer
							</a>
                            <i class="fa fa-angle-right"></i>
						</li>
                        <li>
							<a href="#">
                                Customer Database
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->

    <!-- BEGIN PAGE CONTENT-->
    <form runat="server">
        <div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Customer Database
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">	
    <asp:GridView ID="GridView1" runat="server" CssClass="table table-striped table-bordered table-advance table-hover" AutoGenerateColumns="False" DataKeyNames="ClientID" DataSourceID="SqlDataSource1" >
        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="ClientID" HeaderText="Client ID" InsertVisible="False" ReadOnly="True" SortExpression="ClientID" />
            <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
            <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
        </Columns>
    </asp:GridView>
                            </div>
                </div>
    </div>

        <div class="form-actions">
            <asp:Button ID="Button1" runat="server" Text="Add Client" CssClass="btn red center-block" OnClick="Button1_Click" />
            </div>
        <br />
        <div class="portlet box yellow">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>More Details
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">           
        <asp:GridView ID="GridView2" runat="server" CssClass="table table-striped table-bordered table-hover" AutoGenerateColumns="False" DataKeyNames="ClientID" DataSourceID="SqlDataSource2" >
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="ClientID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ClientID" />
                <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
                <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                <asp:BoundField DataField="Number" HeaderText="Number" SortExpression="Number" />
                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                <asp:BoundField DataField="Gender" HeaderText="Gender" SortExpression="Gender" />
                <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                <asp:BoundField DataField="RegistrationDate" HeaderText="Date" SortExpression="RegistrationDate" />
                <asp:BoundField DataField="About" HeaderText="About" SortExpression="About" />
            </Columns>
        </asp:GridView>          
        </div>
                            </div>
            </div>
        <div class="form-actions">                 
        <asp:Button ID="Button2" runat="server" Text="Export to Word" CssClass="btn red center-block" OnClick="Button2_Click" />
        </div>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [ClientID], [FirstName], [LastName] FROM [CLITABLE]" OldValuesParameterFormatString="original_{0}">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT * FROM [CLITABLE] WHERE ([ClientID] = @ClientID)" ConflictDetection="CompareAllValues" DeleteCommand="DELETE FROM [CLITABLE] WHERE [ClientID] = @original_ClientID AND (([FirstName] = @original_FirstName) OR ([FirstName] IS NULL AND @original_FirstName IS NULL)) AND (([LastName] = @original_LastName) OR ([LastName] IS NULL AND @original_LastName IS NULL)) AND (([Number] = @original_Number) OR ([Number] IS NULL AND @original_Number IS NULL)) AND (([Gender] = @original_Gender) OR ([Gender] IS NULL AND @original_Gender IS NULL)) AND (([Address] = @original_Address) OR ([Address] IS NULL AND @original_Address IS NULL)) AND (([RegistrationDate] = @original_RegistrationDate) OR ([RegistrationDate] IS NULL AND @original_RegistrationDate IS NULL)) AND (([About] = @original_About) OR ([About] IS NULL AND @original_About IS NULL)) AND (([Email] = @original_Email) OR ([Email] IS NULL AND @original_Email IS NULL))" InsertCommand="INSERT INTO [CLITABLE] ([FirstName], [LastName], [Number], [Gender], [Address], [RegistrationDate], [About], [Email]) VALUES (@FirstName, @LastName, @Number, @Gender, @Address, @RegistrationDate, @About, @Email)" OldValuesParameterFormatString="original_{0}" UpdateCommand="UPDATE [CLITABLE] SET [FirstName] = @FirstName, [LastName] = @LastName, [Number] = @Number, [Gender] = @Gender, [Address] = @Address, [RegistrationDate] = @RegistrationDate, [About] = @About, [Email] = @Email WHERE [ClientID] = @original_ClientID AND (([FirstName] = @original_FirstName) OR ([FirstName] IS NULL AND @original_FirstName IS NULL)) AND (([LastName] = @original_LastName) OR ([LastName] IS NULL AND @original_LastName IS NULL)) AND (([Number] = @original_Number) OR ([Number] IS NULL AND @original_Number IS NULL)) AND (([Gender] = @original_Gender) OR ([Gender] IS NULL AND @original_Gender IS NULL)) AND (([Address] = @original_Address) OR ([Address] IS NULL AND @original_Address IS NULL)) AND (([RegistrationDate] = @original_RegistrationDate) OR ([RegistrationDate] IS NULL AND @original_RegistrationDate IS NULL)) AND (([About] = @original_About) OR ([About] IS NULL AND @original_About IS NULL)) AND (([Email] = @original_Email) OR ([Email] IS NULL AND @original_Email IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_ClientID" Type="Int32" />
                <asp:Parameter Name="original_FirstName" Type="String" />
                <asp:Parameter Name="original_LastName" Type="String" />
                <asp:Parameter Name="original_Number" Type="String" />
                <asp:Parameter Name="original_Gender" Type="String" />
                <asp:Parameter Name="original_Address" Type="String" />
                <asp:Parameter Name="original_RegistrationDate" Type="String" />
                <asp:Parameter Name="original_About" Type="String" />
                <asp:Parameter Name="original_Email" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="FirstName" Type="String" />
                <asp:Parameter Name="LastName" Type="String" />
                <asp:Parameter Name="Number" Type="String" />
                <asp:Parameter Name="Gender" Type="String" />
                <asp:Parameter Name="Address" Type="String" />
                <asp:Parameter Name="RegistrationDate" Type="String" />
                <asp:Parameter Name="About" Type="String" />
                <asp:Parameter Name="Email" Type="String" />
            </InsertParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="GridView1" Name="ClientID" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="FirstName" Type="String" />
                <asp:Parameter Name="LastName" Type="String" />
                <asp:Parameter Name="Number" Type="String" />
                <asp:Parameter Name="Gender" Type="String" />
                <asp:Parameter Name="Address" Type="String" />
                <asp:Parameter Name="RegistrationDate" Type="String" />
                <asp:Parameter Name="About" Type="String" />
                <asp:Parameter Name="Email" Type="String" />
                <asp:Parameter Name="original_ClientID" Type="Int32" />
                <asp:Parameter Name="original_FirstName" Type="String" />
                <asp:Parameter Name="original_LastName" Type="String" />
                <asp:Parameter Name="original_Number" Type="String" />
                <asp:Parameter Name="original_Gender" Type="String" />
                <asp:Parameter Name="original_Address" Type="String" />
                <asp:Parameter Name="original_RegistrationDate" Type="String" />
                <asp:Parameter Name="original_About" Type="String" />
                <asp:Parameter Name="original_Email" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>

        </form>

</asp:Content>
