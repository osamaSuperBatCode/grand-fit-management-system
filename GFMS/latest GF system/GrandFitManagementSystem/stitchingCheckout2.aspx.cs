﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GrandFitManagementSystem
{
    public partial class stitchingCheckout2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            date.Text = DateTime.Now.ToString("dd MMM yyyy");

            lblite1.Text = Request.QueryString["ite1"];
            lblite2.Text = Request.QueryString["ite2"];
            lblite3.Text = Request.QueryString["ite3"];

            lblpat1.Text = Request.QueryString["pat1"];
            lblpat2.Text = Request.QueryString["pat2"];
            lblpat3.Text = Request.QueryString["pat3"];

            lblqua1.Text = Request.QueryString["qua1"];
            lblqua2.Text = Request.QueryString["qua2"];
            lblqua3.Text = Request.QueryString["qua3"];

            lbluni1.Text = Request.QueryString["uni1"];
            lbluni2.Text = Request.QueryString["uni2"];
            lbluni3.Text = Request.QueryString["uni3"];

            lbltot1.Text = Request.QueryString["tot1"];
            lbltot2.Text = Request.QueryString["tot2"];
            lbltot3.Text = Request.QueryString["tot3"];

            lblsubtot.Text = Request.QueryString["gratot"];

        }
    }
}