﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace GrandFitManagementSystem
{
    public partial class stitchingCheckout : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection("server=WS141\\MSSQLSERVER2012;database=hrmdb;user id=hrmdbadmin;password=sqlserver");

        double getGrandTotal1 = 0, getGrandTotal2 = 0, getGrandTotal3 = 0, grandTotal;

        double itemqua1 = 0, itemuni1 = 0, tot1;
        double itemqua2 = 0, itemuni2 = 0, tot2;
        double itemqua3 = 0, itemuni3 = 0, tot3;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM SUPTABLE", con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                ddlpat1.DataSource = dt;
                ddlpat1.DataTextField = "SKU";

                ddlpat2.DataSource = dt;
                ddlpat2.DataTextField = "SKU";

                ddlpat3.DataSource = dt;
                ddlpat3.DataTextField = "SKU";
                
                DataBind();
            }
        }

        protected void ddlpat1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM SUPTABLE WHERE SKU='" + ddlpat1.Text + "'", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            lblqua1.Text = dt.Rows[0][1].ToString();
            lblnew1.Text = dt.Rows[0][2].ToString();
        }
        
        protected void btntot1_Click(object sender, EventArgs e)
        {
            itemqua1 = Convert.ToDouble(tbqua1.Text);
            itemuni1 = Convert.ToDouble(tbuni1.Text);
            tot1 = itemqua1 * itemuni1;
            lbltot1.Text = Convert.ToString(tot1);

            //maintain quantity
            double qunty = Convert.ToDouble(tbqua1.Text);
            double stock = Convert.ToDouble(lblqua1.Text);
            double chgstock = stock - qunty;

            con.Open();
            SqlCommand cmd1 = new SqlCommand("UPDATE SUPTABLE SET Quantity=@quantity WHERE ID=@Id", con);
            cmd1.Parameters.AddWithValue("@quantity", chgstock);
            cmd1.Parameters.AddWithValue("@Id", lblnew1.Text);
            cmd1.ExecuteNonQuery();
            con.Close();
            
        }
        protected void ddlpat2_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM SUPTABLE WHERE SKU='" + ddlpat2.Text + "'", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            lblqua2.Text = dt.Rows[0][1].ToString();
            lblnew2.Text = dt.Rows[0][2].ToString();
        }
        
        protected void btntot2_Click(object sender, EventArgs e)
        {
            itemqua2 = Convert.ToDouble(tbqua2.Text);
            itemuni2 = Convert.ToDouble(tbuni2.Text);
            tot2 = itemqua2 * itemuni2;
            lbltot2.Text = Convert.ToString(tot2);

            //maintain quantity
            double qunty = Convert.ToDouble(tbqua2.Text);
            double stock = Convert.ToDouble(lblqua2.Text);
            double chgstock = stock - qunty;

            con.Open();
            SqlCommand cmd1 = new SqlCommand("UPDATE SUPTABLE SET Quantity=@quantity WHERE ID=@Id", con);
            cmd1.Parameters.AddWithValue("@quantity", chgstock);
            cmd1.Parameters.AddWithValue("@Id", lblnew2.Text);
            cmd1.ExecuteNonQuery();
            con.Close();

            
        }
        protected void ddlpat3_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM SUPTABLE WHERE SKU='" + ddlpat3.Text + "'", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            lblqua3.Text = dt.Rows[0][1].ToString();
            lblnew3.Text = dt.Rows[0][2].ToString();
        }
        protected void btntot3_Click(object sender, EventArgs e)
        {
            itemqua3 = Convert.ToDouble(tbqua3.Text);
            itemuni3 = Convert.ToDouble(tbuni3.Text);
            tot3 = itemqua3 * itemuni3;
            lbltot3.Text = Convert.ToString(tot3);

            //maintain quantity
            double qunty = Convert.ToDouble(tbqua3.Text);
            double stock = Convert.ToDouble(lblqua3.Text);
            double chgstock = stock - qunty;

            con.Open();
            SqlCommand cmd1 = new SqlCommand("UPDATE SUPTABLE SET Quantity=@quantity WHERE ID=@Id", con);
            cmd1.Parameters.AddWithValue("@quantity", chgstock);
            cmd1.Parameters.AddWithValue("@Id", lblnew3.Text);
            cmd1.ExecuteNonQuery();
            con.Close();

            

        }
        protected void btngratot_Click(object sender, EventArgs e)
        {
            getGrandTotal1 = Convert.ToDouble(lbltot1.Text);
            getGrandTotal2 = Convert.ToDouble(lbltot2.Text);
            getGrandTotal3 = Convert.ToDouble(lbltot3.Text);

            grandTotal = getGrandTotal1 + getGrandTotal2 + getGrandTotal3;

            lblgra.Text = Convert.ToString(grandTotal); 

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/stitchingCheckout2.aspx?ite1=" + ddlite1.Text + "&ite2=" + ddlite2.Text + "&ite3=" + ddlite3.Text +
                "&pat1=" + ddlpat1.Text + "&pat2=" + ddlpat2.Text + "&pat3=" + ddlpat3.Text +
                "&qua1=" + tbqua1.Text + "&qua2=" + tbqua2.Text + "&qua3=" + tbqua3.Text +
                "&uni1=" + tbuni1.Text + "&uni2=" + tbuni2.Text + "&uni3=" + tbuni3.Text +
                "&tot1=" + lbltot1.Text + "&tot2=" + lbltot2.Text + "&tot3=" + lbltot3.Text
                + "&gratot=" + lblgra.Text);
        }


        protected void btnreg1_Click(object sender, EventArgs e)
        {
            con.Open();
            string str = "INSERT INTO STIT_ITEM_SALES(Item, Fabric, Quantity, UnitCost, Total, OrderDate, DeliveryDate, Description, CustomerName, CustomerContact, Status) values('" + ddlite1.Text + "','" + ddlpat1.Text + "','" + tbqua1.Text + "','" + tbuni1.Text + "','" + lbltot1.Text + "','" + tbord.Text + "','" + tbdel.Text + "','" + tbdes.Text + "','" + tbcus.Text + "','" + tbcuscon.Text + "','" + ddlsta.Text + "')";
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            con.Close();
            lblrow1.Text = "Record 1 Inserted";
            lblrow1.Visible = true;
        }
        protected void btnreg2_Click(object sender, EventArgs e)
        {
            con.Open();
            string str = "INSERT INTO STIT_ITEM_SALES(Item, Fabric, Quantity, UnitCost, Total, OrderDate, DeliveryDate, Description, CustomerName, CustomerContact, Status) values('" + ddlite2.Text + "','" + ddlpat2.Text + "','" + tbqua2.Text + "','" + tbuni2.Text + "','" + lbltot2.Text + "','" + tbord.Text + "','" + tbdel.Text + "','" + tbdes.Text + "','" + tbcus.Text + "','" + tbcuscon.Text + "','" + ddlsta.Text + "')";
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            con.Close();
            lblrow2.Text = "Record 2 Inserted";
            lblrow2.Visible = true;
        }

        protected void btnreg3_Click(object sender, EventArgs e)
        {
            con.Open();
            string str = "INSERT INTO STIT_ITEM_SALES(Item, Fabric, Quantity, UnitCost, Total, OrderDate, DeliveryDate, Description, CustomerName, CustomerContact, Status) values('" + ddlite3.Text + "','" + ddlpat3.Text + "','" + tbqua3.Text + "','" + tbuni3.Text + "','" + lbltot3.Text + "','" + tbord.Text + "','" + tbdel.Text + "','" + tbdes.Text + "','" + tbcus.Text + "','" + tbcuscon.Text + "','" + ddlsta.Text + "')";
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            con.Close();
            lblrow3.Text = "Record 3 Inserted";
            lblrow3.Visible = true;

        }
        protected void Button4_Click(object sender, EventArgs e)
        {

        }
        protected void Button7_Click(object sender, EventArgs e)
        {

        }

       
    }
}