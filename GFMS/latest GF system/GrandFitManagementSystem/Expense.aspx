﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="Expense.aspx.cs" Inherits="GrandFitManagementSystem.Expense" %>
<asp:Content ID="Content1" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Stitching Purchases <small>order details</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Expenses
							</a>
                            <i class="fa fa-angle-right"></i>
						</li>
							
                        <li>
							<a href="Expenses.aspx">
								Expenses Database
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
    <form runat="server">
    <div>
        <h3>Expenses Records</h3>
            <hr />
            <table class="table table-bordered table-hover table-striped table-condensed flip-content">
                <tr>
                    <td class="text-center" style="width: 150px">
                        <h5>Expense On</h5>
                    </td>
                    <td class="text-center" style="width: 150px">
                        <h5>Expense Cost</h5>
                    </td>
                    <td class="text-center" style="width: 150px">
                        <h5>Invoice Number</h5>
                    </td>
                    <td class="text-center" style="width: 150px">
                        <h5>Action</h5>
                    </td>
                </tr>
                <tr>
                    <td class="input-xsmall" style="width: 150px">
                        <asp:TextBox ID="tbexpon" CssClass="form-control" placeholder="details" runat="server"></asp:TextBox>
                    </td>
                    <td class="input-small" style="width: 150px">
                        <asp:TextBox ID="tbexpcos" CssClass="form-control" placeholder="price" runat="server"></asp:TextBox>
                    </td>
                    <td class="input-small" style="width: 150px; text-align: center">
                        
                        <asp:TextBox ID="tbinv" CssClass="form-control" placeholder="number" runat="server"></asp:TextBox>
                        
                    </td>
                    <td class="input-small" style="width: 150px; text-align: center">
                        <asp:Button ID="Button1" runat="server" CssClass="btn red" Text="Submit" OnClick="Button1_Click" />
                    </td>
                </tr>
                </table>

        </div>
        </form>
</asp:Content>
