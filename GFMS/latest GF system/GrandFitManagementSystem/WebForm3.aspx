﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="WebForm3.aspx.cs" Inherits="GrandFitManagementSystem.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">
        <div>
    <asp:GridView ID="gvImages" CssClass="Gridview" runat="server" AutoGenerateColumns="False" 

HeaderStyle-BackColor="#7779AF" HeaderStyle-ForeColor="white">

<Columns> 

<asp:BoundField HeaderText = "Image Name" DataField="imagename" />

<asp:TemplateField HeaderText="Image">

<ItemTemplate>

<asp:Image ID="Image1" runat="server" ImageUrl='<%# "ImageHandler.ashx?ImID="+ Eval("ImageID") %>' Height="150px" Width="150px"/>

</ItemTemplate>

</asp:TemplateField>

</Columns>

</asp:GridView>
</div>
    </form>
</asp:Content>
