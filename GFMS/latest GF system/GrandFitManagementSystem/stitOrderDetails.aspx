﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterPage.Master" AutoEventWireup="true" CodeBehind="stitOrderDetails.aspx.cs" Inherits="GrandFitManagementSystem.stitOrderDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="sidebar" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Stitching Purchases <small>order details</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.aspx">
								DashBoard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Stitching Purchases
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
    <form runat="server">
        <hr />
        <div>
            <h3>Search Column</h3>
            <asp:TextBox ID="tbsearch" runat="server" width="70%" CssClass="form-control" placeholder="search..."></asp:TextBox>
            <div>
                <asp:Button ID="Button1" CssClass="btn red"  runat="server" Text="Submit" OnClick="Button1_Click" />
            </div>
        </div>
        <br />
        <div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Stitching Order Purchases Records
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
        <asp:GridView ID="GridView1" runat="server" CssClass="table table-hover table-bordered table-striped table-condensed flip-content" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="SqlDataSource1" AllowPaging="True" AllowSorting="True">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                <asp:BoundField DataField="Item" HeaderText="Item" SortExpression="Item" />
                <asp:BoundField DataField="Fabric" HeaderText="Fabric" SortExpression="Fabric" />
                <asp:BoundField DataField="Total" HeaderText="Total" SortExpression="Total" />
                <asp:BoundField DataField="OrderDate" HeaderText="OrderDate" SortExpression="OrderDate" />
                <asp:BoundField DataField="DeliveryDate" HeaderText="DeliveryDate" SortExpression="DeliveryDate" />
                <asp:BoundField DataField="CustomerName" HeaderText="CustomerName" SortExpression="CustomerName" />
                <asp:BoundField DataField="CustomerContact" HeaderText="CustomerContact" SortExpression="CustomerContact" />
                <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
            </Columns>
        </asp:GridView>
                            </div>
                    </div>
            </div>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" DeleteCommand="DELETE FROM [STIT_ITEM_SALES] WHERE [ID] = @original_ID AND (([Item] = @original_Item) OR ([Item] IS NULL AND @original_Item IS NULL)) AND (([Fabric] = @original_Fabric) OR ([Fabric] IS NULL AND @original_Fabric IS NULL)) AND (([Total] = @original_Total) OR ([Total] IS NULL AND @original_Total IS NULL)) AND (([OrderDate] = @original_OrderDate) OR ([OrderDate] IS NULL AND @original_OrderDate IS NULL)) AND (([DeliveryDate] = @original_DeliveryDate) OR ([DeliveryDate] IS NULL AND @original_DeliveryDate IS NULL)) AND (([CustomerName] = @original_CustomerName) OR ([CustomerName] IS NULL AND @original_CustomerName IS NULL)) AND (([CustomerContact] = @original_CustomerContact) OR ([CustomerContact] IS NULL AND @original_CustomerContact IS NULL)) AND (([Description] = @original_Description) OR ([Description] IS NULL AND @original_Description IS NULL)) AND (([Status] = @original_Status) OR ([Status] IS NULL AND @original_Status IS NULL)) AND (([Quantity] = @original_Quantity) OR ([Quantity] IS NULL AND @original_Quantity IS NULL))" InsertCommand="INSERT INTO [STIT_ITEM_SALES] ([Item], [Fabric], [Total], [OrderDate], [DeliveryDate], [CustomerName], [CustomerContact], [Description], [Status], [Quantity]) VALUES (@Item, @Fabric, @Total, @OrderDate, @DeliveryDate, @CustomerName, @CustomerContact, @Description, @Status, @Quantity)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT [ID], [Item], [Fabric], [Total], [OrderDate], [DeliveryDate], [CustomerName], [CustomerContact], [Description], [Status], [Quantity] FROM [STIT_ITEM_SALES]" UpdateCommand="UPDATE [STIT_ITEM_SALES] SET [Item] = @Item, [Fabric] = @Fabric, [Total] = @Total, [OrderDate] = @OrderDate, [DeliveryDate] = @DeliveryDate, [CustomerName] = @CustomerName, [CustomerContact] = @CustomerContact, [Description] = @Description, [Status] = @Status, [Quantity] = @Quantity WHERE [ID] = @original_ID AND (([Item] = @original_Item) OR ([Item] IS NULL AND @original_Item IS NULL)) AND (([Fabric] = @original_Fabric) OR ([Fabric] IS NULL AND @original_Fabric IS NULL)) AND (([Total] = @original_Total) OR ([Total] IS NULL AND @original_Total IS NULL)) AND (([OrderDate] = @original_OrderDate) OR ([OrderDate] IS NULL AND @original_OrderDate IS NULL)) AND (([DeliveryDate] = @original_DeliveryDate) OR ([DeliveryDate] IS NULL AND @original_DeliveryDate IS NULL)) AND (([CustomerName] = @original_CustomerName) OR ([CustomerName] IS NULL AND @original_CustomerName IS NULL)) AND (([CustomerContact] = @original_CustomerContact) OR ([CustomerContact] IS NULL AND @original_CustomerContact IS NULL)) AND (([Description] = @original_Description) OR ([Description] IS NULL AND @original_Description IS NULL)) AND (([Status] = @original_Status) OR ([Status] IS NULL AND @original_Status IS NULL)) AND (([Quantity] = @original_Quantity) OR ([Quantity] IS NULL AND @original_Quantity IS NULL))" >
            <DeleteParameters>
                <asp:Parameter Name="original_ID" Type="Int32" />
                <asp:Parameter Name="original_Item" Type="String" />
                <asp:Parameter Name="original_Fabric" Type="String" />
                <asp:Parameter Name="original_Total" Type="Decimal" />
                <asp:Parameter DbType="Date" Name="original_OrderDate" />
                <asp:Parameter DbType="Date" Name="original_DeliveryDate" />
                <asp:Parameter Name="original_CustomerName" Type="String" />
                <asp:Parameter Name="original_CustomerContact" Type="String" />
                <asp:Parameter Name="original_Description" Type="String" />
                <asp:Parameter Name="original_Status" Type="String" />
                <asp:Parameter Name="original_Quantity" Type="Decimal" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Item" Type="String" />
                <asp:Parameter Name="Fabric" Type="String" />
                <asp:Parameter Name="Total" Type="Decimal" />
                <asp:Parameter DbType="Date" Name="OrderDate" />
                <asp:Parameter DbType="Date" Name="DeliveryDate" />
                <asp:Parameter Name="CustomerName" Type="String" />
                <asp:Parameter Name="CustomerContact" Type="String" />
                <asp:Parameter Name="Description" Type="String" />
                <asp:Parameter Name="Status" Type="String" />
                <asp:Parameter Name="Quantity" Type="Decimal" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Item" Type="String" />
                <asp:Parameter Name="Fabric" Type="String" />
                <asp:Parameter Name="Total" Type="Decimal" />
                <asp:Parameter DbType="Date" Name="OrderDate" />
                <asp:Parameter DbType="Date" Name="DeliveryDate" />
                <asp:Parameter Name="CustomerName" Type="String" />
                <asp:Parameter Name="CustomerContact" Type="String" />
                <asp:Parameter Name="Description" Type="String" />
                <asp:Parameter Name="Status" Type="String" />
                <asp:Parameter Name="Quantity" Type="Decimal" />
                <asp:Parameter Name="original_ID" Type="Int32" />
                <asp:Parameter Name="original_Item" Type="String" />
                <asp:Parameter Name="original_Fabric" Type="String" />
                <asp:Parameter Name="original_Total" Type="Decimal" />
                <asp:Parameter DbType="Date" Name="original_OrderDate" />
                <asp:Parameter DbType="Date" Name="original_DeliveryDate" />
                <asp:Parameter Name="original_CustomerName" Type="String" />
                <asp:Parameter Name="original_CustomerContact" Type="String" />
                <asp:Parameter Name="original_Description" Type="String" />
                <asp:Parameter Name="original_Status" Type="String" />
                <asp:Parameter Name="original_Quantity" Type="Decimal" />
            </UpdateParameters>
        </asp:SqlDataSource>

        <div class="portlet box yellow">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Search Results
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
        <asp:GridView ID="GridView2" runat="server" CssClass="table table-striped table-bordered table-hover" ></asp:GridView>

                                </div>
                            </div>
            </div>
        </form>
</asp:Content>
